package com.web.wps.util.upload.minio;

import com.web.wps.propertie.MinioProperties;
import com.web.wps.util.file.FileType;
import com.web.wps.util.file.FileTypeJudge;
import com.web.wps.util.file.FileUtil;
import com.web.wps.util.upload.ResFileDTO;

import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
public class MinioUtil {
    private final MinioProperties minio;

    private final FileTypeJudge fileTypeJudge;

    @Autowired
    public MinioUtil(MinioProperties minio, FileTypeJudge fileTypeJudge) {
        this.minio = minio;
        this.fileTypeJudge = fileTypeJudge;
    }

    private MinioClient getOSSClient() {
        return new MinioClient(this.minio.getEndpoint(), this.minio.getAccessKey(), this.minio.getAccessSecret());
    }

    public void deleteFile(String key) {
        MinioClient client = getOSSClient();
        try {
            System.out.println("delete-getBucketName" + this.minio.getBucketName());
            System.out.println("delete-diskname:" + this.minio.getDiskName());
            System.out.println("delete-key:" + key);
            client.removeObject(this.minio.getBucketName(), String.valueOf(this.minio.getDiskName()) + key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String uploadFile(InputStream inputStream, String fileName, long fileSize, String bucketName, String diskName, String localFileName) {
        String resultStr = null;
        try {
            MinioClient minioClient = new MinioClient(this.minio.getEndpoint(), this.minio.getAccessKey(), this.minio.getAccessSecret());
            ObjectWriteResponse objectWriteResponse = minioClient.putObject((PutObjectArgs) ((PutObjectArgs.Builder) ((PutObjectArgs.Builder) ((PutObjectArgs.Builder) PutObjectArgs.builder()
                    .bucket(bucketName))
                    .stream(inputStream, inputStream.available(), -1L)
                    .object(fileName))
                    .bucket(bucketName))
                    .build());
            resultStr = objectWriteResponse.etag();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultStr;
    }

    public ResFileDTO uploadMultipartFile(MultipartFile file) {
        String fileType, fileName = file.getOriginalFilename();
        ResFileDTO o = new ResFileDTO();
        long fileSize = file.getSize();
        try {
            InputStream inputStream = file.getInputStream();
            FileType type = this.fileTypeJudge.getType(inputStream);
            if (type == null || "null".equals(type.toString()) ||
                    "XLS_DOC".equals(type.toString()) || "XLSX_DOCX".equals(type.toString()) ||
                    "WPSUSER".equals(type.toString()) || "WPS".equals(type.toString())) {
                fileType = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            } else {
                fileType = type.toString().toLowerCase();
            }
        } catch (Exception e) {
            e.printStackTrace();
            fileType = "";
        }
        try {
            o = uploadDetailInputStream(file.getInputStream(), fileName, fileType, fileSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return o;
    }

    public ResFileDTO uploadDetailInputStream(InputStream in, String fileName, String fileType, long fileSize) {
        String uuidFileName = FileUtil.getFileUUIDName(fileName, fileType);
        String fileUrl = this.minio.getFileUrlPrefix() + this.minio.getDiskName() + uuidFileName;
        String md5key = uploadFile(in, uuidFileName, fileSize, this.minio.getBucketName(),
                "", fileName);
        ResFileDTO o = new ResFileDTO();
        if (md5key != null) {
            o.setFileType(fileType);
            o.setFileName(fileName);
            o.setCFileName(uuidFileName);
            o.setFileUrl(fileUrl);
            o.setFileSize(fileSize);
            o.setMd5key(md5key);
        }
        return o;
    }

}